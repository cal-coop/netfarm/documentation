#lang scribble/base

@require["../spec-macros.scrbl"]
@require[scribble-math/dollar]
@title{Systems}

A @concept{system} handles the requests sent to a node in a distributed
system, and sends requests for blocks it has not yet retrieved to synchronise
itself. A @concept{block} is a tuple consisting of a @italic{name},
@italic{version}, @italic{channel set}, and some @italic{data}; of which the
@concept{metadata} can be used by a system to determine what it must retrieve
and update.

@definitions{
@defprotoclass{system}

The protocol class for a @term{system}. A system is maintained by some
@concept{maintenance threads}. One of which is the @concept{leader thread},
which runs a @concept{leader loop} that schedules requests for synchronisation
and stops itself when the system is stopped.

@todo{The standard-system should track maintenance threads like it tracks the
  leader thread, and eventually, this should all be integrated into the
  @italic{bailout} thread supervision library.}
}

@definitions{
@defclass["standard-system"]{system}

A system that uses the database and synchronisation protocols to implement the
basic behaviour of a node.
}

@definitions{
@defclass["echo-system"]{system}

A silly system that responds to any messages other than invalid syntax with
the message sent.
}

@definitions{
@definitarg{:id}

@defreader["system-id"]{system}

The ID of a system, as an integer in the range @${[0, 2^{256})}
}

@definitions{
@definitarg{:acceptors}

The acceptors of a system.
}
