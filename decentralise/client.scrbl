#lang scribble/base

@require["../spec-macros.scrbl" scribble-math/dollar]
@title{Clients}

A @concept{client} abstracts over a connection by replacing the
asynchronous, multiplexed connection protocol with a synchronous
protocol.

@section{Client types}

@definitions{
@defprotoclass{client}

@defclass["connection-client"]{client}

A client which uses a connection to perform actions.

}

@definitions{
@definitarg{:connection}

@defreader["client-connection"]{client}

The connection the client uses to perform actions.

}

@definitions{
@defclass["kademlia-client"]{client}

A client which uses multiple connections to perform actions, deciding
on which connections to use and searching for more connections using
the Kademlia distributed hash table search algorithm.
}

@section{Actions}

Asynchronous execution is handled using an @concept{action} protocol.
An action is some computation that can be composed (in a method similar
to @term{monads} in functional programming), and run later. Actions have
some benefits over some other asynchronous protocols, such as callbacks:

@itemlist[
@item{Actions are ``first-class'' values, and can be manipulated before
  they are run.}
@item{Actions can be composed without threading a callback through values,
  i.e. we write @cl{(then (f) (g))} instead of @cl{(f :then (g))}. This
  is not a big deal for small examples, but threading the callback through
  abstracting functions gets annoying quickly.}
@item{Complex actions can be made using something like @term{do notation}.}
]

@definitions{
@defmacro["chain"]{@&body binding* return-form}

A macro which chains some actions to create a more complex action.

Each binding is either of the form
@code-template{(@var{v} <- @var{a})},
indicating that the value of the action should be bound to a variable, or
@code-template{(@var{v} = @var{e})}, indicating that the value
of a form should be bound to a variable. The package of @cl{<-} or
@cl{=} does not matter, much like the keywords in extended @cl{loop}
(but @cl{:<-} doesn't look very nice, and @cl{:=} wouldn't really
re-assign anything). The value of the action is produced by the
@italic{return-form}.

The method for expanding @cl{chain} is quite similar to the method for
desugaring @term{do notation}. The expansion of @cl{chain} is defined by
a recursive function:

@tabular[#:sep (hspace 1)
         #:column-properties '(right center left)
         @list[@list[@list{E[@code-template{(chain @var{value})}]} "=" "value"]
               @list[@list{E[@code-template{(chain (@var{v} = @var{e}) . @var{rest})}]}
                     "=" @code-template{(let ((@var{v}@var{ }@var{e})) (chain . @var{rest}))}]
               @list[@list{E[@code-template{(chain (@var{v} <- @var{a}) . @var{rest})}]}
                     "=" @code-template{(then @var{a} (lambda (@var{v}) (chain . @var{rest})))}]]
        ]
}


@definitions{
@defun["run"]{action}

Run an action, either returning the value it succeeded with, or signalling
the condition it failed with.

@defun["run-away"]{action}

Run an action in the background, returning nothing of interest immediately.
}

@definitions{
@defgeneric["%run"]{action success failure}

Implement @cl{run} by setting up the action to be run somewhere, which
should end up calling either @cl{success} with a value, or @cl{failure} with
a condition. This probably should not block. 
}


@subsection{Basic actions}

We have picked these actions as primitives to compose other actions out of.
It is possible to implement every other action in the client, in terms of
these actions, but this is not necessary. (@note{We don't.})

@definitions{
@defun["finish"]{value}

An action which always returns @emph{value}. This function is named
@tt{return} in Haskell.
}

@definitions{
@defun["functional-action"]{function}

An action which calls @emph{function} with a success continuation and a
failure continuation. If the function calls the success continuation with
a value, it succeeds with that value. If the function calls the failure
continuation with a value, it fails with that value. If the function
signals an error, the action fails with that error.
}

@subsection{Derived actions}

From these basic actions, it is possible to derive more useful actions.

@definitions{
@defun["then"]{action action-producer}

Produce an action which runs @emph{action}, then calls
@emph{action-producer} with the value @emph{action} produced to produce
another action, then runs that action.

@cl{then} could be implemented so that @cl{(then a p)} @${\equiv}
@cl{(functional-action (lambda (s f) (%run a (lambda (v) (%run (funcall p v) s f)) f)))}.
This function is named @cl{>>=} in Haskell.
}

@definitions{
@defun["then*"]{action1 action2}

Produce an action which runs @emph{action1} and discards its result,
then calls @emph{action2} and produces its result.

@cl{then*} could be implemented so that @cl{(then* a1 a2)} @${\equiv}
@cl{(then* a1 (constantly a2))}. This function is named @cl{>>} in
Haskell.
}

@definitions{
@defun["parallel"]{actions}

Run a sequence of actions in parallel. If all actions succeed, this action
succees with a vector of all the values of the actions, or if one action
fails, this action fails with the first condition.

Evidently @cl{(parallel empty-sequence)} @${\equiv} @cl{(finish #())}, and
@cl{(parallel (singleton-sequence-of a))} @${\equiv}
@cl{(chain (x <- a) (finish (vector a)))}. The other cases are much more
hairy.
}

@definitions{
@defun["attempt"]{action failure-producer}

Similar to @cl{then}, except that instead @cl{failure-producer} will be
called with a failure condition, and the action will succeed with the
value of calling @cl{failure-producer}.

The implementation can be similar to that of @cl{then}, but extending the
failure continuation instead of the success continuation.
}

@subsection{Client actions}

@definitions{
@defgeneric["get-block*"]{client name @&key @&allow-other-keys}

An action which retrieves a block from a client.
}

@definitions{
@defgeneric["put-block*"]{client name version channels data @&key fail-if-too-old @&allow-other-keys}

An action which puts a block to a client. If @emph{fail-if-too-old} is true,
we expect that this block should be the newest version, and fail if it is
too old. If @emph{fail-if-too-old} is false, then we consider the action
successful if a node has a newer version of the block..
}
