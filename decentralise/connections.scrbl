#lang scribble/base

@require["../spec-macros.scrbl"]
@title{Connections}

A @concept{connection} connects a @term{client} and a @term{server} together,
allowing the two to exchange @term{messages} between each other. decentralise2
is able to handle many different types of connections, including in-memory
@term{passing connections} and @term{socketed connections} that use the
operating system's networking capabilities, which only have to implement a
simple protocol.

@definitions{
@defprotoclass{connection}

The protocol class of a connection.
Note that a connection is implicitly started when it is instantiated.
}

@definitions{
@defgeneric["stop-connection"]{connection}

Stop the connection, which should free any resources that creating the
connection acquired, such as threads and sockets. This generic function
uses the @cl{progn} method combination, and thus every method should be
qualified with @cl{progn} (or @cl{:around}).
}

@definitions{
@defgeneric["handle-message"]{connection message}

Handle a message received by a connection.

@defmethod["handle-message"]{(connection connection) message}

Call the @term{message handler} with the message provided. 
}

@definitions{
@definitarg{:message-handler}

@defaccessor["connection-message-handler"]{connection}
@definition-section["Arguments"]{
  When setting this place, the new value must be a function which
  accepts a message as argument.
}

The @concept{message handler}, a function that is called with
each received message. The default handler will wait for a new handler
to be set, in order to prevent some race conditions; where a message
could be received, before the client has set a message handler.
}

@definitions{
@defgeneric["write-message"]{connection message}

Write a message to a connection. This may be called by multiple threads;
it is expected that nothing strange will occur if two threads call
@cl{write-message} simultaneously.
}

@section{Connection types}

The types of data that a connection can send are represented by a connection's
class, not unlike the stream classes such as @cl{fundamental-character-stream}
and @cl{fundamental-binary-stream} in the Gray streams de-facto standard.

Note that, unlike Gray streams, the protocol does not distinguish between
bidirectional and unidirectional connections. It is, of course, possible to
create connections which never receive messages, and/or do nothing when writing
messages.

@definitions{
@defprotoclass{character-connection}

A connection that can send blocks with vectors of characters (strings) as data.

@defprotoclass{binary-connection}

A connection that can send blocks with vectors of octets as data.
}

@section{Threaded connections}

@definitions{
@defprotoclass{threaded-connection}

A connection that uses a thread to read messages.
}

@definitions{
@defgeneric["read-message"]{connection}

Read a message from the connection, blocking until one is present, then
returning it and @cl{T}, or return something and @cl{NIL} if a message
cannot be read ever again (e.g the other node closed the connection).
}

@definitions{
@defgeneric["listener-loop"]{connection}

The thread of the connection will call this function, and stop when it returns.
}

@subsection{Socketed connections}

@definitions{
@defclass["socketed-connection"]{threaded-connection}

A connection that uses a thread reading a @cl{usocket} socket to read messages.
}

@definitions{
@definitarg{:socket}

@defreader["connection-socket"]{connection}

The socket used by a socketed connection.
}

@definitions{
@definitarg{:stream}

@defreader["connection-stream"]{connection}

The stream used by a socketed connection. This is provided separately to
the socket, as an implementor may want to wrap the stream in a SSL stream.
}

@definitions{
@defmethod[#:qualifier "Around" "read-message"]{(connection socketed-connection)}

This method simplifies the protocol somewhat. If the next applicable method
returns a message @italic{m}, then this method will return the values @italic{m}
and @cl{T}. If the method signals an error of type @cl{end-of-file} (suggesting
the peer closed the connection), the connection is stopped, and this method
returns the values @cl{NIL} and @cl{NIL}. The primary method for
@cl{read-message} with a @cl{socketed-connection} just has to attempt to read
from the socket, and return a message.
}

@section{Inbuilt connection types}

@definitions{
@defclass["netfarm-format-connection" "threaded-connection" "character-connection"]

A @term{character connection} that exchanges messages using a simple textual
format, based on the wire protocol @term{Nettle} used. The URI protocol name of a
@cl{netfarm-format-connection} is @cl{netfarm}.

As the name suggests, this was intended to be the default connection class for
Netfarm; but the creation of Netfarm's much more efficient binary format
suggests that we should use a binary connection class as default.

Nonetheless, it is quite easy to read, and superficially looks like the HTTP/1.0
wire protocol. It is also wrapped in an SSL connection. Messages begin with
unique ``verbs'', and are separated by new lines.
}

@bnf[
  @rule["boolean" @tt{yes} @tt{no}]
  @rule["integer" "(0 ... 9)*"]
  @rule["length-prefixed-string" @list["integer " @tt{:} " character*"]]

  @rule["block-name"   "length-prefixed-string"]
  @rule["channel-name" "length-prefixed-string"]
  @rule["id"           "length-prefixed-string"]
  @rule["line-count"   "integer"]
  @rule["reason"       "length-prefixed-string"]

  @rule["announce"     @list[@tt{announce} " boolean"]]
  @rule["block-header" @list[@tt{block} " block-name version line-count channel-name*"]]
  @rule["error"        @list[@tt{error} " block-name reason"]]
  @rule["get"          @list[@tt{get} " block-name*"]]
  @rule["ok"           @list[@tt{ok}  " block-name"]]
  @rule["node"         @list[@tt{node} " uri id"]]
  @rule["subscribe"    @list[@tt{subscribe} " channel-name*"]]
  @rule["subscription" @list[@tt{subscription} " block-name version channel-name*"]]
]

The base decentralise2 messages require little effort to translate
into this grammar. In the following description, we consider
translations where all value names are the same to be @italic{trivial}.
The translation rules for each message are as follows:

@itemlist[
@item{@cl{(:get names)} is represented by the rule @italic{get}, with
  @italic{block-name*} being all of @cl{names}.}
  
@item{@cl{(:block block-name version channels data)} is mostly represented by the rule
  @italic{block-header}.
  Let @italic{line-count} be the number of lines in @cl{data} (0 if @cl{data}
  is empty, or one more than the count of @cl{#\Newline}
  characters in @cl{data} otherwise).
  The rule @italic{block-header} is used, with @italic{channel-name*} being all
  of @cl{channels}, and other rules matched to variables with the same names.
  @italic{line-count} lines of @cl{data} then succeed the block header.}
  
@item{@cl{(:ok name)} and @cl{(:error name reason)} are represented by the rules
  @italic{ok} and @italic{error} respectively
  trivially.}
  
@item{@cl{(:subscription name version channels)} is represented by the rule
  @italic{subscription} mostly trivially, with @italic{channel-name*} being all
  of @cl{channels}.}

@item{@cl{(:subscribe channels)} is represented by the rule @italic{subscribe},
  with @italic{channel-name*} being all of @cl{channels}.}

@item{@cl{(:allow-announcement allow?)} is represented by the rule
  @italic{announce}, with @italic{boolean} being @tt{yes} if @cl{allow?} is
  true, or @tt{no} if it is false.}

@item{@cl{(:announce uri id)} is represented by the rule @italic{node}
  trivially.}
]

@definitions{

@defclass["netfarm-binary-connection" "threaded-connection"
          "binary-connection" "character-connection"]

A @term{character connection} that exchanges messages using a simple binary
format. It can be seen that the same abstract grammars from
@cl{netfarm-format-connection} are used in the wire protocol of a
@cl{netfarm-binary-connection}. Again, this connection is wrapped in an SSL
connection. The URI protocol name of a @cl{netfarm-binary-connection} is
@cl{nbinary}.

@bnf[
  @rule["boolean" @tt{00} @tt{01}]
  @rule["byte"    @list{@tt{00} ... @tt{ff}}]
  
  @rule["long-integer"  "byte × 8"]
  @rule["short-integer" "byte"]
  
  @rule["long-bytes"   "long-integer byte*"]
  @rule["long-string"  "long-bytes"]
  @rule["short-bytes"  "short-integer byte*"]
  @rule["short-string" "short-bytes"]
  @rule["name-list" "long-integer" "short-string"]

  @rule["block-name" "short-string"]
  @rule["block-list" "name-list"]
  @rule["channel-list" "name-list"]
  @rule["version" "long-integer"]
  @rule["metadata" "block-name long-integer channel-list"]
  @rule["uri" "short-string"]
  @rule["id" "short-string"]

  @tag-rule["get"             "01" "block-list"]
  @tag-rule["character-block" "02" "metadata long-string"]
  @tag-rule["binary-block"    "03" "metadata long-bytes"]
  @tag-rule["ok"              "04" "block-name"]
  @tag-rule["error"           "05" "block-name long-string"]
  @tag-rule["subscribe"       "06" "block-list"]
  @tag-rule["subscription"    "07" "metadata"]
  @tag-rule["allow-announcement" "08" "boolean"]
  @tag-rule["announce"        "09" "uri id"]
]
}

@section{Connection URIs}

A @concept{uniform resource identifier} is used to describe how to connect to
a node. decentralise2 currently allows specifying a protocol and host name in
a uniform resource identifier (@todo{should it also include a port number?}),
and can connect to a node by its URI.

@definitions{
@defreader["connection-uri"]{connection}

A URI that could possibly be used to create a connection to the node the
connection is connected to (or @cl{nil}).

@defmethod["connection-uri"]{(connection socketed-connection)}

The URI is created by appending the protocol name and remote address of a
socketed connection, by means of
@cl{(format nil "~a:~a" (connection-uri-protocol-name connection) (connection-address connection))}
}

@definitions{
@defreader["connection-uri-protocol-name"]{socketed-connection}

The protocol name of a connection.
}

@definitions{
@defreader["connection-address"]{socketed-connection}

@definitarg{:address}

The remote address of a connection. If an address is not provided when a
socketed connection is created, it will be retrieved from the socket object
provided.
}

@definitions{
@defun["connection-from-uri"]{uri @&rest initargs}

Create a connection to a URI. If a function is associated with the provided
protocol name, it is called with the hostname, and all other initialization
arguments. Otherwise, an error is signalled.

@defmacro["define-protocol"]{protocol-name creator}

Associate a function with a protocol name.
}
