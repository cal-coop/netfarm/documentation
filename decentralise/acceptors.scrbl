#lang scribble/base

@require["../spec-macros.scrbl"]
@title{Acceptors}

An @concept{acceptor} creates connections by accepting them from something that
faces the outside world, such as a server socket, and gives them to a system.

@definitions{
@defprotoclass{acceptor}

The protocol class for an acceptor.

@defclass["threaded-acceptor"]{acceptor}

The protocol class for an acceptor that uses a thread to give a system
connections.
}

@section{Acceptor protocol}

@definitions{
@defgeneric["give-system-connection"]{system connection}

Give a connection to the system the acceptor accepts connections for.
}

@definitions{

@defgeneric["start-acceptor"]{acceptor system}

Start the acceptor, allowing it to create connections on behalf of the system.
An acceptor is likely to use auxilliary methods for this generic function.

@defmethod["start-acceptor"]{(acceptor threaded-acceptor) system}

Start a thread that repeatedly accepts connections by calling
@cl{acceptor-loop}.
}

@definitions{
@defgeneric["stop-acceptor"]{acceptor}

Stop the acceptor, preventing it from creating more connections.
An acceptor is likely to use auxilliary methods for this generic function.

@defmethod["stop-acceptor"]{(acceptor acceptor)}

This method does nothing.
}

@section{Threaded acceptors}

@definitions{
@defgeneric["accept-connection"]{(acceptor)}

Return a new connection that should be added to the acceptor's system.
An acceptor must use its own primary methods for this generic function.
}

@definitions{
@defgeneric["acceptor-loop"]{acceptor}

@defmethod["acceptor-loop"]{(acceptor acceptor)}

Forever accept connections and add them to a system.
}

@subsection{Socket acceptor}

@definitions{
@defclass["socket-acceptor"]{threaded-acceptor}

An acceptor that accepts connections from a socket, also wrapping the
connections in SSL.

@definitarg{:connection-class}

The class to make instances of. This must be a subclass of either
@cl{character-connection} or @cl{binary-connection}.

@definitarg{:host}

@definitarg{:port}

The hostname and port to bind to. These default to @cl{"0.0.0.0"} and
@cl{1892}, respectively.

@definitarg{:certificate}

@definitarg{:key}

Pathnames for the @term{certificate} and @term{key files}.
}

@definitions{
@defmethod["start-acceptor" #:qualifier ":Before"]{(acceptor socket-acceptor) system}

This method starts listening on the given hostname and port.

@defmethod["stop-acceptor" #:qualifier ":Before"]{(acceptor socket-acceptor) system}

This method stops listening on the given hostname and port.

@defmethod["accept-connection"]{(acceptor socket-acceptor) system}

A socket method accepts a @term{connection} by accepting a connection from the
socket, and instantiating an instance of the acceptor's connection class.

Connections are instantiated with initargs @cl{:socket} and @cl{:stream},
with values for the raw socket and the SSL-wrapped stream, respectively.
}
