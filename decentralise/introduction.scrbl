#lang scribble/base

@require["../spec-macros.scrbl"]
@title{decentralise2}

The first component that was developed for Netfarm was the
@italic{decentralise} library, which allows a programmer to implement some kind of
distributed network by writing the components that they are most interested in,
and using provided implementations of the components they are not interested in
writing.

A @italic{decentralise} user may implement one or more of:

@itemlist[
@item{a system, which handles synchronisation and retrieval and storage
  of data,}
@item{an acceptor, which accepts connections from the outside world,
  such as over the Internet by binding a socket,}
@item{a connection, which sends and receives messages, and}
@item{a message type, which represents something that can be sent on a
  connection.}
]

In a peer-to-peer context, servers and clients do not exist, as there is no
distinction between them. This is still true for decentralise2, as
servers do not distinguish between servers, clients, and any other program
creating connections and sending and receiving messages. However, the client is
written to retrieve specific objects for a user and provide a synchronous
interface to them, and a system retrieves many objects asynchronously, and
stores them in some kind of database; so it may be argued that there is
something that looks like a client, and something that looks like a server.

On the other hand, a server also may handle accepting connections and parsing
messages, and thus a system would not be a server, as it does not do those
things; so we should stick with the term @term{system} to be precise in what
a component does.

@include-section["connections.scrbl"]
@include-section["messages.scrbl"]
@include-section["acceptors.scrbl"]
@include-section["systems.scrbl"]
@include-section["standard-system.scrbl"]
@include-section["client.scrbl"]
