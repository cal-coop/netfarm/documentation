#lang scribble/base

@require["../spec-macros.scrbl"]
@title{Messages}

A @concept{message} is an abstract object that is sent over a @term{connection},
and can be considered the lowest-level communication protocol decentralise2 is
concerned with.

@definitions{
@defmacro["message-case"]{message @&body case*}

@definition-section["Syntax"]{
@bnf[
  @rule["case" @list{@tt{(} pattern form* @tt{)}}]
  @rule["pattern" @list{@tt{(} keyword variable* @tt{)}}]
  @rule["dont-care" @list{@tt{~} | @tt{-} | @tt{_} | @tt{NIL}}]
  @rule["variable" @list{dont-care | symbol}]
]
}

Match the value of @cl{message} against some patterns, which look like
the arguments to the function @cl{message}, and evaluate the forms of the
first matching case, with all variables that don't match the
rule @italic{dont-care} bound to the arguments.
}

@definitions{
@defun["message"]{keyword @&rest value*}

Create a message from the type designated by the keyword with the given values.
The number of values must be the same as the number of accessors of the type.
}

@section{Built in message types}

@(define (defmtype struct-name keyword values)
   (defthing struct-name (list "(" (tt keyword) " " values ")") "Message Type"))

@definitions{
@defmtype["get-blocks" ":get"]{names}

@defmtype["put-block" ":block"]{name version channels data}

@defmtype["ok-response" ":ok"]{name}

@defmtype["error-response" ":error"]{name reason}

@defmtype["new-subscriptions" ":subscription"]{name version channels}

@defmtype["subscription" ":subscribe"]{channels}

@defmtype["announcement-control" ":allow-announcement"]{allow?}

@defmtype["node-announcement" ":announce"]{uri id}
}

@section{Defining message types}

@definitions{
@defmacro["define-message-type"]{keyword type-specifier constructor-name @&rest accessors}

Define a message type named by the given keyword, which corresponds to the given
Common Lisp type specifier, can be constructed by calling the function named by
the @cl{constructor-name}, and can be destructured using all the functions named
in @cl{accessors}.
}

@section{Translators}

The content of messages (particuarly @cl{:block} messages) is often subject to
@concept{translation}, so that logic in a system can be separated cleanly from
serializing and deserializing for connections.

@definitions{
@defgeneric["object->data"]{object target source}

``Serialize'' or ``render'' an object, which is an instance of a type
denoted by the source, to be sent to the target.
}

@definitions{
@defgeneric["data->object"]{data source target}

``Deserialize'' or ``parse'' the data received from a source, into an
object which is an instance of a type denoted by the target.

Note that all three arguments should be specialized, else weird things can occur.
For example, an @cl{ansible-connection} may be both a @cl{binary-connection} and
a @cl{character-connection}, so it is possible it will receive either character
or binary messages. If you write methods to parse some data with specializers
@cl{(t binary-connection target)} and @cl{(t character-connection target)},
either method may be selected, based on the class precedence list of
@cl{ansible-connection}, independent of the type of data in the message; and the
wrong parser may be used. Instead, you should specialize on
@cl{(string character-connection target)} and
@cl{(vector binary-connection target)}, so that the correct method will be
selected.
}
