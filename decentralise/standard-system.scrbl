#lang scribble/base

@require["../spec-macros.scrbl" scribble-math/dollar]
@title{Standard system behaviour}

@section{Standard system database protocol}

Note that all generic functions specified in this section may signal a
condition of any type, as well as any specified condition types. 

@definitions{
@defgeneric["get-block"]{system name}

Retrieve the information about a block named @cl{name}.

@definition-section["Return Values"]{
  version, channels, data: the values of a block.
}

@definition-section["Exceptional Situations"]{
  If no block named @cl{name} exists, a condition of type @cl{not-found}
  is signalled.
}
}

@definitions{
@defgeneric["put-block"]{system name version channels data}

Store block information in a system, potentially overwriting an old block
with the same name.

It is expected that @cl{get-block} will be able to observe the new block
immediately after @cl{put-block} returns.

}

@definitions{
@defgeneric["map-blocks"]{function system}

Call the @cl{function} with the metadata of every block stored.

@cl{map-blocks} may provide stale metadata if @cl{put-block} is called
while @cl{map-blocks} is providing metadata.

@definition-section["Arguments"]{
  The @cl{function} is a function which accepts a string name, list of
  string channels, and a non-negative integer version.
}
}

@definitions{
@defclass["memory-database-mixin"]{}

This mixin class implements the database protocol using a concurrent
hash table.
}

@definitions{
@defmacro["with-consistent-block"]{(name system) @&body body}

Take exclusive access of the data of a block, preventing the data from
being changed by other threads which use @cl{with-consistent-block} or
@cl{with-consistent-blocks}.

@defmacro["with-consistent-blocks"]{(names system) @&body body}

Take exclusive access of the data of a list of blocks, preventing the
data from being changed by other threads which use
@cl{with-consistent-block} or @cl{with-consistent-blocks}.

It is an error to nest uses of @cl{with-consistent-block} and/or
@cl{with-consistent-blocks}, i.e. to use either macro in a dynamic
extent where exclusive access of blocks is already established.

(This restriction was made because attempting to materialise locks on
the fly, rather than using a fixed table of locks like our
implementation, is relatively much slower, requires more
synchronisation and more bug prone. Fortunately, we have not found a
need to nest uses of these macros.)

All users of block-manipulating constructs must use
@cl{with-consistent-block} for it to be useful. All the callers of
@cl{get-block} and @cl{put-block} in the decentralise2 standard system
implementation use @cl{with-consistent-block}, and the scope of it will
be documented if it is non-trivial.

}
@section{Message interpretation}

Each standard message type is interpreted as follows:

@itemlist[
@item{@cl{:block}: If the name of the block provided is the name of a
  special block, the @cl{:put} function for that special block is called.
  Otherwise, if the system already has stored a block with the given name,
  and the version of the block provided is not greater than the version
  of the block stored by the system, an error with reason @cl{too old} is
  sent. If the provided block is otherwise newer, then the serialised data
  is translated into an object using @cl{data->object}, the block is stored
  using @cl{put-block}, and then its metadata is broadcast to subscribed
  connections.}
  
@item{@cl{:get}: For each name provided, the system either responds with
  a corresponding @cl{:block} message with the contents of a block, or
  an @cl{:error} message with the same name, and some reason as to why
  the block could not be retrieved. The contents of each block are translated
  for the connection using @cl{object->data}.

  If a block names a special block, the @cl{:get} function for the
  special block is called. Otherwise, the block data is retrieved using
  @cl{get-block}.

  In either case, a @cl{:ok} message is sent if no error is signalled.
  Otherwise, @cl{describe-decentralise-error} is called to produce a
  reason string, and a @cl{:error} message is sent instead.

  Blocks requested are held consistent until responses are sent, in
  order to prevent a class of race conditions. Such a condition may be
  caused when a response is generated, then an update is broadcasted,
  and then the now-stale response is sent, overriding the update. With
  the block held until the response is sent, either the response is
  sent before the update, or the response is generated after the update
  is broadcasted.}
  
@item{@cl{:allow-announcement}: If the message prevents announcement,
  then information about the connection will no longer appear in
  @cl{nodes} listings. If the message allows announcement, but the
  connection has not provided an ID, an @cl{:error} with reason
  @cl{need id first} is sent. If the message allows announcement, and the
  connection has provided an ID, then information about the connection will
  appear in @cl{nodes} listings.}
@item{@cl{:subscribe}:}
@item{@cl{:subscription}:}
]

@todo{A subclass of @cl{standard-system} could handle @cl{:announce} in
      order to implement @term{peer discovery}.}

@definitions{
@defgeneric["system-translation-target"]{system}

@defmethod["system-translation-target"]{(system standard-system)}

The third argument to use when translating by calling
@cl{object->data} and @cl{data->object}. The default method returns
the symbol @cl{t}.
}

@section{Special blocks}

@define[@defspecialblock[x] @defthing[x "" "Special block"]]

Retrieving and storing (using @cl{:get} and @cl{:block} messages,
respectively, with the names of) some blocks from a @term{standard
  system} will cause them to perform some special behaviour that does
not invoke @cl{get-block} or @cl{put-block} as per normal.

@definitions{
@defmacro["define-special-block"]{(class-name block-name @&optional (data-type-name @cl{t}))
                                  @&key get put}

Define a special block for instances of a class, with associated get and
put functions.

@note{Because we use generic functions to implement special block
      dispatch, it is possible to use @cl{call-next-method}.
      Should this be standard behaviour?}

The data returned by the get function is translated by
@cl{object->data}, with the given @cl{data-type-name} as source, and
the connection as target. The data given as an argument to the put
function is translated by @cl{data->object}, with the connection as
source, and the given @cl{data-type-name} as target.
}

The standard system provides some special blocks:

@definitions{
@defspecialblock{list}

This block contains a list of every block name stored (excluding special
block names).
}

On a character connection, the list is encoded with the metadata of a
block per line, each line containing the length-prefixed name, the
version number, and then length-prefixed channels, somewhat mirroring
the syntax of the header of @cl{:block} messages when sent from a
@cl{netfarm-format-connection}.

On a binary connection, the list is encoded as repeating metadata
records. These records do not have any delimiters between them; as
their length can be detected from the record data. Each record
contains the name encoded as a short-string, the version as a
long-integer, the number of channels as a long-integer, then each
channel name as a short-string.

More formally, both encodings can be generated with the following grammars,
building upon the wire protocol rules in @italic{Connections}:

@bnf[
  @rule["block-metadata" "block-name version channel-name*"]
  @rule["character-block-listing"  "empty-string" "(block-metadata newline)* block-metadata"]

  @rule["metadata-record" "block-name version channel-list"]
  @rule["binary-block-listing"  "metadata-record*"]
]

When a standard system receives a listing, it assumes that the blocks in
the listings may be retrieved from the client, and it may send @cl{:get}
requests for those blocks. If the listing cannot be parsed, a system will
instead respond with an @cl{:error} message, for the block @cl{list}, and
with the reason @cl{invalid listing}.

@definitions{
@defspecialblock{id}

This block contains the ID of the node.

On a character connection, the ID is encoded as a 64-character hexadecimal
string, with uppercase @cl{A} through @cl{F} characters, and the most
significant digit written first. For example, the ID @cl{9781484261347}
would be encoded as
53 zeroes then @cl{8E56DE50FE3}.

@todo{Add a binary encoding. Currently, all our binary connection classes
  are also character connections, and are thus capable of sending character
  blocks, but this may not be ideal. On the other hand, a 50% reduction
  of the size of a 64-byte block that is only sent once is not anything to
  write home about.}

When a standard system receives an ID, it associates that ID with the
connection used to send the ID. This ID appears in node listings, should
the connection then allow itself to be @term{announced}.
}

@definitions{
@defspecialblock{nodes}

This block contains a listing of all nodes that have announced themselves
to the system, with each node record separated by a newline. Each item
in the listing has a length-prefixed URI, which may be used to connect to
a node, a space, and its ID.

@todo{Add a binary encoding. Again, this block is only retrieved once (as
  nodes are announced after that with another message type), but it would
  still be nice to have a binary encoding.}
}


@section{Synchronisation protocol}

The aim of a system is to schedule requests to retrieve the highest
versions of all interesting blocks.

@subsection{Interesting and uninteresting sets}

Block metadata is conceptually stored interesting and uninteresting
sets. As formerly described, requests are scheduled from the
@concept{interesting set}. The @concept{uninteresting set} contains
block metadata which may become interesting, but should not yet be
retrieved.

The sets are defined by two predicates:

@definitions{
@defgeneric["interesting-block-p"]{system name version channels}

Test if block metadata is interesting.

@defgeneric["uninteresting-block-p"]{system name version channels}

Test if block metadata is uninteresting.
}

Note that @cl{uninteresting-block-p} is only ever called with block
metadata that is not interesting.

However, it would be inefficient to constantly test membership of all
known block metadata, so instead metadata is internally stored in two
set data structures. Thus it is necessary to have a protocol to
invalidate the sets.

@definitions{
@defgeneric["update-system-for-new-interesting-block-predicate"]{system maybe-interesting-set maybe-uninteresting-set}

Invalidate parts of, or all of, the interesting and uninteresting
sets.

@definition-section["Arguments"]{
The @cl{now-interesting-set} is a @term{set specifier}. All members of
the set in the uninteresting set which satisfy
@cl{interesting-object-p} are moved to the interesting set.

The @cl{now-uninteresting-set} is a @term{set specifier}. All members of
the set in the interesting set which do not satisfy
@cl{interesting-object-p} are moved to the uninteresting set.
}

@todo{Some way to remove metadata completely?}
}

@subsection{Block predicates}

The interesting set and uninteresting set for a system
are represented as predicates, which usually should not change.

When the interesting set must be changed, the system is notified using a
function which takes two @concept{set specifier}s. A @term{set specifier} is
either:

@itemlist[
@item{a list of block names that are new to that set, or}
@item{@cl{t} if all cached blocks should be sorted again, or}
@item{a predicate that is satisfied by block names that are new to that set}
]

The latter is very useful when sets are sufficiently nearly infinite,
such as in a @term{distributed hash table} where the change, maybe
from ``rehashing'' some types of distributed hash tables, or the
operator deciding they should commit to storing a different partition
of the world, can well exceed @${2^{100}}, and is impractical to
represent as a pair of lists.

@subsection{Tuning throughput}

@definitions{
@definitarg{:concurrent-requests}

The maximum number of concurrent requests that the scheduler will
make.
}

@definitions{
@definitarg{:timeout}

The time to wait for a response to a request in seconds.
}
