#lang scribble/base

@require["../spec-macros.scrbl"]
@title{Conventions}


@section{Strings}

Strings are always encoded using UTF-8 in the Netfarm systems. No buts, no ifs
there. The sequence of @term{code points} used to form a string must be
preserved in encoding and decoding.

(I believe the Common Lisp @italic{babel} library preserves code points, as the
encoding and decoding functions are quite simple, but some guarantees on how code
points are treated would be nice.)

@section{Integers}

Integers are encoded in binary data in big endian format, i.e. such that
the most significant octet (or other unit) is written first, and the least
significant is written last. This format is most common in networking
applications, and is the most convenient for users of human languages that
write left-to-right to read.

Octet vectors are written in hexadecimal, with the first octet to be written
or read appearing at the leftmost position in a vector, and the last at the
right.

For example, @tt{10 01} is the vector consisting of the octets 16 and 1,
and can be interpreted as the integer 4097.
