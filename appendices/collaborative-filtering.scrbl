#lang scribble/base

@require[scribble-math/dollar]
@require["../spec-macros.scrbl" "../bibliography.rkt"]
@title{A description of a voting-web system}

We have not thoroughly tested this voting-web system, but we should describe
it, because it is a very useful but infrequently applied technique for
moderating an online system. This system may be seen as an extension of
many @italic{Usenet} clients' @concept{kill lists}, which contain a set of
patterns that match posts the user does not want to see; but it can also be
used as a whitelist, or just to rank some objects in some order.

It is very important that a distributed system has a means of distributed
``moderation''. We wonder why distributed systems are built without
distributed moderation of some form, and we believe that using centralised
moderation on such a system would be an equivalent of the ``people's stick''
described in @~cite[statism-and-anarchy]. Unlike some kinds of moderation in the
real world, we are not required to give computers some kind of ``objective''
truth, and we do not attempt that with Netfarm. Freeing ourselves from that
obligation allows us to approximate the desires of each user with greater
precision.

This system allows a user to maintain a map of ``facts'' that they assert to
be true or false. A @term{presentation script} may query the voting-web system
while rendering an object, to decide what related objects it should render, or
a client program may just refuse to render some presentation forms of an object
that has certain facts about it asserted.

In the absence of a fact asserted by the user, we believe it is possible to
determine the probability that the user would assert the fact, as a function of
some delegates' assertions. Suppose @${\text{Pr}(F \mathrel{\text{is true to}} U)}
is the probability that a user @${U} takes the fact @${F} to be true, and that
@${\text{delegates}(U)} denotes the set of delegates of the user @${U}. We can
define the probability to be:

@$${
\begin{aligned}
  \text{Pr}(F \mathrel{\text{is true to}} U) &= 0 \text{\ (if U asserts F to be false)} \\
  \text{Pr}(F \mathrel{\text{is true to}} U) &= 1 \text{\ (if U asserts F to be true)} \\
  \text{Pr}(F \mathrel{\text{is true to}} U) &=
       \frac{\displaystyle \sum_{D \in \text{delegates}(U)} Pr(F \mathrel{\text{is true to}} D) \text{correlation}(U, D)}
            {\displaystyle \sum_{D \in \text{delegates}(U)} |\text{correlation}(U, D)|}
\end{aligned}
}
