#lang scribble/base

@require["../spec-macros.scrbl" "../bibliography.rkt" scribble-math/dollar]
@title{Script execution}

The script machine is a stack machine with a representation of lexical
environments and automatic memory management. The major changes to the
@term{SECD machine} @~cite[secd-machine] are additional registers for
convenience, and to facilitate method calls between multiple objects,
and a more common bytecode-based instruction format, rather than an
instruction format based on S-expressions.

@(define (defrecord . registers)
   (tabular #:column-properties '(right left left)
            #:row-properties '((top-border bottom-border) ())
            #:sep (hspace 1)
            (cons @list[@bold{Symbol} @bold{Name} @bold{Purpose}]
                  registers)))
@(define (register variable name . purpose)
   (list ($ variable) name purpose))

@section{Structures}

We introduce the @term{script machine} structure and @term{procedure} structure,
which otherwise do not exist in the Netfarm type and class systems. A script
object, however, is just an instance of @cl["inbuilt@schema"].

@landscape{

The script machine has these registers:

@defrecord[
  @register["S" "Store"]{A stack containing intermediate data.}
  @register["E" "Environment"]{The lexical environment, represented as a list of
                                 vector "frames".}
  @register["P" "Program counter"]{The position the interpreter is reading instructions from.}
  @register["D" "Dump"]{A stack telling the interpreter where to return to.}
  @register["G" "Globals"]{A vector of global variables.}
  @register["C" "Control"]{The script the interpreter is current reading instructions from.}
  @register["A" "Capabilities"]{Flags that allow the machine to perform operations that
                               only make sense in some situations.}
  @register["O" "Self"]{The current object (or the empty list, if we have not called a
                        method).}
  @register["O_2" "Sender"]{The last object, which was the value of @${O}
                            before calling a method.}
  @register["F" "Side effects"]{Effects that the interpreter is going to make
                                on the outside world.}
]

A script has these values:

@defrecord[
  @register["P" "Program"]{An octet vector that encodes instructions to execute.}
  @register["E" "Entry points"]{A vector of descriptions for procedures, which are
                                combined with environments by some
                                instructions to produce procedures.}
  @register["M" "Methods"]{A vector of descriptions for methods.}
  @register["G" "Globals"]{A vector of the initial values of global variables.}
]

A procedure has these values:

@defrecord[
  @register["C" "Control"]{}
  @register["E" "Environment"]{}
  @register["P" "Program counter"]{}
  @register["A" "Argument count"]{}
  @register["O" "Object"]{}
]
}

@section{Notation}

@(define (secd . stuff) @${\left\langle @stuff \right\rangle})
@(define (transition from to . rest)
   (centered-block (para from (hspace 1) @${\rightarrow\allowbreak} (hspace 1) to)
                   rest))
@(define (stackdesc from to . rest)
   (centered-block (para ($ from "\\ \\text{---}\\ " to))
                   rest))
@(define (conjunct . text)
   (for/list ([x text])
     (list ($ (list "{} \\land " x))
           (linebreak))))

We refer to the state of a running interpreter with a set of variables
(as described in the first table). We write the most common registers
in a tuple like @secd{S, E, P, D}, but we do not include all registers
in the tuple because we seldom use the others.

We write the states of a halted interpreter as @${@tuple["halt"]{S, F}},
should it halt correctly, or @${@tuple["error"]{message, cause}}
should it fail. (Only the data stack and side effects are preserved
when an interpreter stops.)

We precede an instruction description with a line like

@(define (definstruction byte name . arguments)
   (defthing (format "~a: ~a" byte name) arguments "Instruction"
             #:index? #f))

@definitions{
@definstruction["N" "NAME" "BYTE-ARGUMENT*"]
}

Such a line means we define an instruction named NAME, which is encoded
starting with the byte N, and then one byte for each of BYTE-ARGUMENT*.

We describe the effects of interpreting an instruction in a method
similar to that of the temporal logic of actions
@~cite[temporal-logic] (but with no temporal expressions): we use a
logical equation where the new registers have "primed" variables,
which look like @${C'}. We write the effects on the tuple of common
registers like

@transition[@secd{a, b, c, d} @secd{e, f, g, h}]

which is shorthand for

@centered-block{
  @${S = a \land E = b \land C = c \land D = d \land
     S' = e \land E' = f \land C' = g \land D' = h}
}

If the equations for an instruction do not reference the primed
variable for a register, the register is left unchanged.

Should an instruction concern itself with only the Store, we further
abbreviate, by writing effects in the form

@stackdesc[@${i_1 \ldots i_m} @${o_1 \ldots o_n}]

(borrowing from Forth conventions) which can be translated to the other
notation as

@transition[@secd{(i_m \ldots i_1\ .\ S), E, P, D}
            @secd{(o_n \ldots o_1\ .\ S), E, P + 1 + bytes, D}]

where @${bytes} is the number of byte arguments to an instruction. Note that
the equivalent transition has the stack prefixes "reversed"; the last
input is popped first, and the first output is pushed first. In Forth,
we may write a program
@tt{3 2 -} (⇒ @tt{1})
and the stack before evaluating @tt{-} as a list would be @tt{(2 3)}.
To maintain a natural argument order for the programmer, the machine
handles arguments "backwards" when described using lists.

To access a value of a structure without destructuring it, we use the notation
@${S_A} to get the value of the variable (slot) @${A} of @${S}. We also use
subscripts to retrieve values from lists and vectors; @${S_n} denotes the
@${n}th value in @${S}. The first element of a sequence is retrieved with a zero
subscript, as with most programming languages; and retrieving an element that
would be past the last element of a sequence causes interpretation to fail.

@(define (tuple name . stuff) @list{\mathrm{@name}[@stuff]})
@(define (proc . stuff) (apply tuple (cons "proc" stuff)))

We create many procedures while running programs, so we write a procedure in
the form @${@proc{C, E, P, A, O}}.

All integer operations are defined to only produce values when the result is
within the bounds of a signed 256-bit integer as previously defined. We define
the set @${Integer} to be @${[-(2^{255}), 2^{255})}.

@section{Execution}

The machine first reads the instruction to execute, by reading the @${P}th octet
in @${C_P}. The instruction arguments are then read similarly, by reading the
@${P + n}th octet in @${C_P} for the @${n}th argument (starting from 1).
Finally, a transition is chosen based on the state of the machine at this
point. Unless the machine has transitioned to a @${halt} state or unwound
entirely to a @${error} state, it then proceeds to execute another instruction.

@margin-note{A poor attempt at specifying that we signal a condition
or throw an error or something - I'm not writing rules to propagate
errors through everything thankyou.}

If a @${unwind} value appears in a computation, the machine attempts
to find the last method call frame produced. If that frame exists, the
machine effectively returns from that frame, but pushes
@${\mathtt{\#f}} on the stack instead of a singleton list. If it does
not exist, the machine transitions to the @${error} state. More formally:

@todo{Write out the recursive function.}

@section{Instructions}

@subsection{Environment}

Variables are indexed by @emph{variable} and @emph{frame} positions; the
newest bindings (which are function arguments) are located where @${frame = 0},
and older bindings have greater frame positions.

We first define a helper function @${getenv} which gets the value in a
particular position in an environment. We also write assignments to this value,
which are shorthand for replacing the value at the designated position in the
environment.

@$${
getenv[env, var, frame] =
\begin{cases}
  F_{var}                        & (F . \_) = env \land frame = 0 \\
  getenv[E, variable, frame - 1] & (\_ . E) = env \land frame > 0 \\
  unwind                         & ()       = env \land frame > 0
\end{cases}
}


@definitions{
@definstruction[1 "get-proc"]{n}

@stackdesc["" @proc{C, E, P_0, A, O}]{where @${(P_0\ A) = (C_E)_n}}

@definstruction[6 "get-proc*"]{n}

@stackdesc["" @proc{C, (), P_0, A, O}]{where @${(P_0\ A) = (C_E)_n}}

Note that @cl{get-proc} closes over the lexical environment, but @cl{get-proc*}
does not; the former would be used for top-level functions, and the latter
would be used for closures.
}

@definitions{
@definstruction[2 "get-value"]{n}

@stackdesc["" "G_n"]

}

@definitions{
@definstruction[3 "set-value!"]{n}
@stackdesc["value" ""]{where @${G_n \leftarrow value}}
}

@definitions{
@definstruction[4 "get-env"]{variable frame}

@stackdesc["" "getenv[E, variable, frame]"]

@definstruction[5 "set-env!"]{variable frame}

@stackdesc["value" ""]{where @${getenv[E, variable, frame] \leftarrow value}}
}

@definitions{
@definstruction[11 "byte"]{value}

@stackdesc["" "value"]
}

@subsection{Control flow}

@definitions{
@definstruction[8 "return"]{}

@transition[@secd{S, E, P, ()} @${@tuple["halt"]{S, A}}]

@transition[@secd{(Value\ .\ \_), \_, \_, (@tuple["normalframe"]{S, E, P, C', O'} . D)}
            @secd{(Value\ .\ S),  E, P, D}]

@transition[@secd{(Value\ .\ \_), \_, \_, (@tuple["methodframe"]{S, E, P, C', O', O_2', F'} . D)}
            @secd{((Value)\ .\ S),  E, P, D}]

The last transition described for @cl{return} handles half of the
unwinding associated with method calls; when it returns from a method
call, it will wrap the value to return in a cons. This serves two
purposes: it prevents the stack layout from changing under the feet of
a script, and it provides an always-correct method to test if a call
was successful.
}

@definitions{
@definstruction[9 "call"]{n}

@; This is far too long to not break manually.
@centered-block{
  @${@secd{(@proc{C', E, P_0, n, O'}\ A_n \ldots A_1\ .\ S), E, P, D}}
  @${\rightarrow}
  
  @secd{(), ((A_1 \ldots A_n)\ .\ E'), P_0,
        (@tuple["normalframe"]{S, E, P + 2, C, O}\ .\ D)}
}

@definstruction[10 "tail-call"]{n}

@centered-block{
  @${@secd{(@proc{C', E, P_0, n, O'}\ A_n \ldots A_1\ .\ S), E, P, D}}
  @${\rightarrow}
  
  @secd{(), ((A_1 \ldots A_n)\ .\ E'), P_0, D}
}

We used to preserve @${S} between function calls, in an attempt to support
writing Forth-ish code, where functions would just manipulate the stack, and
ignore the environment. However, arbitrary control over the stack gets confusing
when higher-order methods are involved; the stack layout after calling the
passed function would be dependent on how the function leaves it, and a
sufficiently clever attacker could use this to corrupt the stack and produce
undesired behaviour.

To "fix" this, we decided that functions always return one value, and cannot
access the data stacks of any other functions, thus the stack layout will always
be known.
}

@definitions{
@definstruction[16 "jump-cond"]{thenhigh thenlow elsehigh elselow}

@transition[@secd{(\mathtt{\#f} . S), E, P, D}
            @secd{S, E, P + 5 + else, D}
           ]{@${{} \land else = elsehigh \times 256 + elselow }}

@transition[@secd{(V\ .\ S), E, P, D}
            @secd{S, E, P + 5 + then, D}
           ]{@${{} \land then = thenhigh \times 256 + thenlow
                   \land V \neq \mathtt{\#f}}}

@definstruction[17 "jump"]{high low}

@transition[@secd{S, E, P, D}
            @secd{S, E, P + 3 + high \times 256 + low, D}]

Jumps are relative to the end of the instruction, so jumping 0 bytes does
nothing.
}

@definitions{
@definstruction[19 "call-method"]{n}

@transition[@secd{(Name\ Object\ A_n \ldots A_1\ .\ S), E, P, D}
            @secd{(), ((Rest\ A_1 \ldots A_n)\ .\ E'), P_0,\allowbreak
                  (@tuple["methodframe"]{S, E, P + 2, C, O, O_2, F}\ .\ D)}]{
  @conjunct[
  @list{@tuple["primary"]{(Primary\ .\ Rest)} = methods[Object, Name]}
  @list{@proc{C', E', P_0, n + 1, O'} = Primary}
  @list{O_2' = O}
  ]
}

@transition[@secd{(Name\ Object\ A_n \ldots A_1\ .\ S), E, P, D}
            @secd{(), ((Rest\ Name\ (A_1 \ldots A_n))\ .\ E'), P_0,\allowbreak
                  (@tuple["methodframe"]{S, E, P + 2, C, O, O_2, F}\ .\ D)}]{
  @conjunct[
  @list{@tuple["doesnotunderstand"]{(Primary\ .\ Rest)} = methods[Object, Name]}
  @list{@proc{C', E', P_0, 3, O'} = Primary}
  @list{O_2' = O}
  ]
}

@stackdesc["A_1\\ \\ldots\\ A_n\\ Object\\ Name" "\\mathtt{\\#f}"
          ]{otherwise}

@todo{Fill in how to compute @${methods}.}

Every script of the class of an object contains a list of lists
@${ (Name\ P_0\ A') }, where @${A'} is the number of "real" arguments.
Methods have an implicit "next method list" argument. When calling a method,
the next method list is the first argument, and every other method is shifted
over one, so the first argument appears in the second position in the new frame.

The global variable vectors for each @${\langle object, script \rangle} pair must
be the same. This ensures that one method on an object and one script can observe
the changes made by another method on that object and that script.

It is usually a good idea to cache computation of @${methods}; method caching
in old Smalltalk was a fairly cheap way to get a reasonable performance gain,
as approximately every method call requires dispatching. This is true to a
lesser extent for Netfarm code, which has fewer, but still many, method
calls.
}

@subsection{Stack control}

@definitions{
@definstruction[34 "drop"]{}

@stackdesc["x" ""]

@definstruction[35 "dup"]{}

@stackdesc["x" "x\\ x"]

@definstruction[36 "swap"]{}

@stackdesc["x\\ y" "y\\ x"]

These instructions function identically to the words defined of the same names
in ANSI Forth.
}

@subsection{Lists}

@definitions{
@definstruction[64 "cons"]{}

@stackdesc["car\\ cdr" "(car\\ .\\ cdr)"]{@${{} \land cdr \in List}}

As we do not have a representation for improper lists, the cdr must be a
list.
}

@definitions{
@definstruction[65 "car"]{}

@stackdesc["(car\\ .\\ \\_)" "car"]{}
}

@definitions{
@definstruction[66 "cdr"]{}

@stackdesc["(\\_\\ .\\ cdr)" "cdr"]{}
}

@definitions{
@definstruction[67 "null"]{}

@stackdesc["()"             "\\mathtt{\\#t}"]{}

@stackdesc["(\\_\\ .\\ \\_)" "\\mathtt{\\#f}"]{}
}

@definitions{
@definstruction[68 "consp"]{}

@stackdesc["x" "x \\in Cons"]{}
}

@definitions{
@definstruction[69 "append"]{}

@; Convince TeX not to parse this as a + +b
@stackdesc["a\\ b" "a {++} b"]{}
}

@subsection{Operators}

@definitions{
@definstruction[71 "equal"]{}

@stackdesc["a\\ b" "equal[a,b]"]

@$${
\begin{aligned}
  equal[(a_a\ .\ a_d), (b_a\ .\ b_d)] &= equal[a_a, b_a] \land equal[a_d, b_d] \\
  equal[(),            ()]            &= \mathtt{\#t} \\
  equal[a \in Integer, b \in Integer] &= a = b \\
  equal[a \in String,  b \in String]  &= a = b \\
  equal[a \in Object,  b \in Object]  &= hash[a] = hash[b] \\
  equal[@proc{a_c, \ldots, a_o}, @proc{a_c, \ldots, a_o}] &= a_c = b_c \land
                                                            \ldots \land
                                                            a_o = b_o \\
  equal[\_,            \_]            &= \mathtt{\#f}
\end{aligned}
}

It is probably wise to use an auxiliary stack when implementing @${equal}, as
the client code may provide arbitrarily large data structures.
}

@definitions{
@definstruction[72 "string="]{}

@stackdesc["a\\ b" "a = b"]{@${{} \land a \in String \land b \in string}}
}

@definitions{
@definstruction[73 "list"]{n}

@stackdesc["e_1 \\ldots e_n" "(e_1 \\ldots e_n)"]{}
}

@definitions{
For @${OP} being each name of the following three instructions:

@definstruction[96 "+"]{}

@definstruction[97 "-"]{}

@definstruction[98 "*"]{}

@stackdesc["a\\ b" "a \\mathop{OP} b"
          ]{@${{} \land a \mathop{OP} b \in Integer}}
}

@definitions{
@definstruction[99 "/"]{}
@stackdesc["a\\ b" "\\lfloor \\frac{a}{b} \\rfloor"
          ]{@${{} \land \lfloor \frac{a}{b} \rfloor \in Integer}}

Flooring is defined to produce the closest integer below the actual value,
e.g @${\lfloor \frac{-5}{2} \rfloor = -3}
}

@definitions{
@definstruction[100 "abs"]{}

@stackdesc["a" "|a|"]{@${{} \land |a| \in Integer}}

Note that @${|a|} can be out of bounds when @${a} is in bounds; which
is when @${a = -(2^{255})}.
}

@definitions{
For @${T} being the type in the name of the following three instructions:

@definstruction[105 "integer?"]{}

@definstruction[106 "list?"]{}

@definstruction[108 "string?"]{}

@stackdesc["o" "o \\in T"]
}

@definitions{
For @${OP} being each name of the following three instructions:

@definstruction[112 "="]{}

@definstruction[113 "<"]{}

@definstruction[114 ">"]{}

@definstruction[115 "/="]{}

@definstruction[116 "<="]{}

@definstruction[117 ">="]{}

@stackdesc["a\\ b" "a \\mathop{OP} b"]{@${{} \land a \in Integer \land b \in Integer}}
}

@subsection{Object instructions}

@definitions{

@definstruction[129 "schema"]{}

@stackdesc["o" "schema[o]"]{@${{} \land o \in Object}}
}

@(define (tt* . stuff)
   (list "\\text{\\texttt{" stuff "}}"))

@definitions{
For @${N} being each name of the following two instructions:

@definstruction[130 "add-computed-value"]{}

@definstruction[140 "remove-computed-value"]{}

@centered-block{
 @${name\ value\ \text{---}}

 @${{} \land @tt*{:write-computed-values} \in A}
 
 @${{} \land F' = ((N\; @tt*{:target}\; O\;
                        @tt*{:slot}\; slot[name, O]\;
                        @tt*{:value}\; value)\; .\; F)}
}
}

@definitions{
@definstruction[131 "object-value"]

@stackdesc["object\\ name" "value[object, slot[name, object]]"]

@definstruction[133 "object-bound?"]

@stackdesc["object\\ name" "bound?[object, slot[name, object]]"]

@definstruction[132 "object-computed-values"]

@stackdesc["object\\ name" "value[object, computedslot[name, object]]"
          ]{when @${@tt*{:read-computed-values} \in A}}

When we figure out how to deal with "plain old data" with no scripts
to provide accessor methods, then we will probably make object-value
and object-computed-values only access slots of the current object; in
order to preserve an @term{object capability model}, where objects can
only send each other references to other objects.
}

@definitions{
@definstruction[134 "object-authors"]

@stackdesc["o" "map[car, signatures[o]]"
          ]{when @${o \in Object}}
}
