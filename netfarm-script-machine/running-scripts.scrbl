#lang scribble/base

@require["../spec-macros.scrbl"]
@title{Running scripts}

@definitions{
@defun["setup-interpreter"]{script arguments @&optional capabilities}

@defun["setup-method-interpreter"]{object method arguments @&optional capabilities}

Make a script machine which will run either a script or call a method on an
object.

If there are no applicable methods (when using @cl{setup-method-interpreter}),
an error will be signalled.

@definition-section["Arguments"]{
  @cl{arguments} is a list of values to pass as arguments, and
  @cl{capabilities} is a capability list.
}
}

@definitions{
@defun["run-interpreter"]{interpreter @&key cycle-limit print-cycles step}

Run an interpreter, returning the final data stack, and the final interpreter
state.

@definition-section["Arguments"]{
  @cl{cycle-limit} is either the limit on ``cycles'' that the script machine can
  execute. (Most instructions take one cycle to execute, but instructions which
  could 
}
}

@definitions{
@defun["run-script"]{script @&rest arguments}

@defun["send-message"]{object method-name @&rest arguments}

@defvar["*capabilities*"]{@cl{()}}

Run a script or send a message, by setting up and then immediately running the
appropriate interpreter.

The script machine will be run with the capability list stored in
@cl{*capabilities*}.
}

@definitions{
@defun["run-script-machines"]{object @&key (apply-side-effects @cl{t})
                                           (recipient-filter @cl{(constantly t)})}

Run the initialization script for an object, returning multiple values
corresponding to the final data stack of the script machine.

@definition-section["Arguments"]{
  When @cl{apply-side-effects} is true, side effects will be applied.
  @cl{recipient-filter} is a predicate which is called with each side effect,
  and only side effects which satisfies the predicate will be applied.

  (Perhaps @cl{:apply-side-effects nil} could be expressed as
   @cl{:recipient-filter (constantly nil)}, but that's a bit less ergonomic.)
}
}
