#lang scribble/base

@require["../spec-macros.scrbl"]
@title{Side effects}

An interpreter may produce @concept{side effects} to interact with other
objects. Currently, there is only one category of side effect: side
effects which modify the @concept{computed value sets} of an object.
A side effect is represented as a list, with a symbol in its @cl{car} and
a property list in its @cl{cdr}, such as
@cl{(add-computed-value :target #<some-object> :name "foo" :value ("bar"))}

The final state of an object is produced by applying all the applicable
side effects. Depending on the nature of the implementation, it may or
may not be appropriate to implement side effects exactly as suggested,
though the implementation must act as if it were implemented as suggested.

The order of which side effects are applied does not affect the produced
object. It is for this reason that Netfarm exhibits
@concept{strong eventual consistency}: how objects are synchronised does
not affect the computation performed, and so all nodes storing an object
will eventually converge on objects with the same values.

@definitions{
@defmacro["with-consistent-state"]{(object) @&body body}

Defer any other threads from applying any side effects to an object, until
the body forms have been evaluated. Other threads calling
@cl{apply-side-effects-on} will not block, but the thread executing in a
@cl{with-consistent-state} form will not observe the effects made.
However, side effects can still be applied immediately in the current thread.

This non-blocking but deferrable behaviour is a requisite to
implementing and using subscribing to side effects in an ergonomic
manner. The thread applying side effects should not block for too long, in
order to maximize throughput, but client threads should not find computed
values changing from under their feet. We store an ``effect log'' in
each object, and write to the log, when a thread already is viewing a
consistent state over it, and update the object when the thread unwinds past
@cl{with-consistent-state}. Short of threads disappearing, by means of
@cl{bt:destroy-thread} or the like, objects will always have side effects
applied eventually, and the updating thread will not block for very long.
}

@definitions{
@defun["apply-side-effects-on"]{object side-effects}

Apply a list of side effects to an object. It is expected that @cl{object}
is the target of all the side effects.
}

@definitions{
@defun["apply-side-effect"]{side-effect}

Apply a side effect. This could be implemented as
@cl{(apply-side-effects-on (getf (rest side-effect) :target) (list side-effect))}
}

@define[(defeffect name . keywords) @defthing[name keywords "Side effect"]]

@section{Computed values}

We maintain a mapping of computed values to counts for every slot of
every object. When all side effects have been considered, the table is
traversed, and each computed value is added according to the count if
it is not negative, and no computed values are added when a count is
negative.

@definitions{
@defeffect["add-computed-value"]{@cl{:target} @cl{:slot} @cl{:value}}

@defeffect["remove-computed-value"]{@cl{:target} @cl{:slot} @cl{:value}}

@definition-section["Arguments"]{

@emph{target} is the object which has affected itself.

@emph{slot} is the @term{effective slot definition} representing the slot
which will be affected.

@emph{value} is the value to be affected.

}

Increment or decrement the count of the given computed value, respectively.
}

A related technique for implementing these effects, which is useful
when saving effects to a disk, is to add each effect to a log, and
then compute the computed values by applying side effects when loading
an object. It is also possible to remove effects that cancel out, and
to compress the result into one final state periodically. However, the
objects-affecting graph would have to be maintained outside the effect
log for it to still be correct.

It is trivial to show that computed values exhibit strong eventual
consistency. For each relevant effect, we add 1 or add -1 to each
counter, and addition of numbers is commutative, i.e. effects can be
performed in any order to retrieve the same result.
