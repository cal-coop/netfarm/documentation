#lang scribble/base

@require[scribble-math/dollar scribble/core scribble/html-properties
         "bibliography.rkt"]

@title{The Netfarm Book}
@author{Applied Language}

@table-of-contents[]

@include-section["introduction.scrbl"]
@include-section["conventions/introduction.scrbl"]
@include-section["decentralise/introduction.scrbl"]
@include-section["netfarm/introduction.scrbl"]
@include-section["netfarm-script-machine/introduction.scrbl"]
@include-section["netfarm-server/introduction.scrbl"]
@include-section["netfarm-client/introduction.scrbl"]
@include-section["appendices/collaborative-filtering.scrbl"]

@index-section[]

@generate-bibliography[]
