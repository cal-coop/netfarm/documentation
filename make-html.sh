#!/bin/sh

scribble --htmls netfarm-book.scrbl
for p in netfarm-book/*.html; do
    sed -i -e 's/<!DOCTYPE[^>]*>/<!DOCTYPE html>/' \
           -e 's/initial-scale=0.8/initial-scale=1.0/' \
           -e "s/{}&rsquo;/{}'/g" \
        "$p"
done
cat spec-macros.css mobile-view.css >> netfarm-book/scribble-style.css
