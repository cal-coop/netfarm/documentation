#!/bin/sh

scribble --prefix tex-prefix.tex --latex netfarm-book.scrbl
sed -i -e 's/^\\renewrmdefault//g' \
    -e 's/^\\packageTxfonts//g' \
    -e 's/\\hspace\*{\\fill}//g' \
    -e 's/\\HR{}\\\\/\\HR{}/g' \
    netfarm-book.tex
sed -i -e 's/^\\\\//g' netfarm-book.tex
pdflatex netfarm-book.tex
makeindex netfarm-book
pdflatex netfarm-book.tex
