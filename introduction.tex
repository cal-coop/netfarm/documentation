\section{Introduction}

\subsection{``Abstract''}

\textbf{Netfarm is an attempt at creating a distributed, trustless object
  system.} We develop Netfarm to fill a perceived void in the rapidly-expanding
``decentralisation'' bubble, to utilise the greater fault tolerance of a
\term{distributed hash table} and simplify programming via a client language's
\term{meta object protocol}, and to devise less hierarchical moderation techniques
that do not leave them at the whim of their servers' operators and administrators.

The design of Netfarm is not ideal due to historical and planning issues; but it
should serve well as an introduction to trustless object systems, which we
believe can be easier to program and have better throughput than some of the models
that are used frequently today, such as blockchains (demonstrated to be usable for
programming in Ethereum) and various federated protocols (such as ActivityPub,
notably used in Mastodon among other ``fediverse'' blogging servers).

To summarise the design, Netfarm implements a \term{synchronous}
\term{message-passing} \term{object system} (like Smalltalk or Self) hosted on
a \term{distributed hash table}. A Netfarm network can reach
\term{eventual consistency} as state is either so unlikely to collide, such as
objects which are stored by hashes, which are probably less likely to collide
than for a computer to incorrectly compute a hash; or it is simple to unify,
such as \term{computed values}. However, Netfarm does not decide on an
ordering of computed values, and so has fairly weak consistency guarantees.

\subsection{How Netfarm deviates from existing designs}

Some of these points are made redundant by the preceding paragraphs, but it may
be worthwhile to explain why we made those claims, and why they are desirable. 

\begin{itemize}
\item The greater fault tolerance of a \term{distributed hash table},
  in contrast to a federated system, is provided by users not having identity or
  state stored in one server (or server identity), so \textbf{the failure of one
    server will not make the network useless for any group of users}.

  It is also not uncommon for federated systems to have clustering problems.
  If the developers of a system run a server in that system, it is likely they
  will encourage users to use their server, or not encourage users to use other
  servers. In one egregious system, the developers \textsl{sell} servers for
  their network. It's likely that they're all hosted in the same data centres;
  what happens if something there goes down? Ouch. This phenomenon is not only
  influenced by the developers, though. When one person tells their friends that
  they are moving onto a federated system, and provide their username (which
  includes the domain name of the server), it is likely their friends will use
  that server too.
  
\item Another reason that federated systems have clustering, and a very
  repugnant problem, is that the roles of community moderation and system
  administration are often confused. With smaller servers, this is not an issue,
  as there may be few enough users to be manageable by the few administrators of
  a server, and the users may share sufficient opinions that allow
  administrators to do relatively little work. However, the largest three
  Mastodon servers have 1.3 million users between them \cite{mastodon-instances},
  so this is not a very good excuse.

  If one is unhappy with the way their server is moderated, some ever-so-wise
  users remind them they are ``free'' to leave for another, which is incredibly
  impolite. We feel obligated to state that it's analogous to "suggesting" that
  one should leave their country if they are not happy with how it is governed,
  which is usually considered by them to be a very lousy argument, and one we
  consider to be similar in sentiment to theirs (except that moving country is
  often harder than moving server).
  
  \textbf{The questions of node operation and moderation in Netfarm are fully
    separated}, allowing users to decide with \textsl{case-by-case granularity}
  on what content they want to see, and to aggregate their peers' decisions with
  various techniques.

\item We are also obligated to mention \term{blockchains}, which are distributed
  transaction logs of sorts. ``Full nodes'' in those systems store almost every
  transaction, whereas \textbf{Netfarm nodes benefit from the use of a
    distributed hash table, in which nodes only store part of the network's
    state.} However, \textbf{mutability in Netfarm is still accomplished with
    ``transactions''} through the use of computed values.
  (The Ethereum system is likely to implement ``sharding'', which splits state
  over the network \cite{ethereum-sharding}, but that technique is very
  different to using a distributed hash table.)

\item \textbf{Netfarm does not prescribe any use-cases, nor is it designed for
    any specific applications.} Using schemas and their scripts, it is
  possible to implement many different programs atop Netfarm.

\item \textbf{The meta-object protocol of the client language makes interaction
    with Netfarm objects trivial}; Netfarm objects are client objects, and
  Netfarm schemas can be defined using client classes.
\end{itemize}

\subsection{The structure of Netfarm}

We see the Netfarm system as a synthesis of various components:

\begin{itemize}
\item A means of creating distributed hash tables (\texttt{decentralise2}),
\item An immutable object system with cryptographic signatures
  (\texttt{netfarm}),
\item A script machine for handling side effects reproducibly
  (\texttt{netfarm-scripts}), and
\item A protocol for presenting objects to a user (\texttt{netfarm-interactor})
\end{itemize}

However, a user should be able to modify and use any component separately, and
should have extensive control over many aspects of the system, to implement any
changes while still acting as a ``client'' of the system, that does not modify
the system itself.

\subsection{Thanks}

We would like to thank
\textsl{Luke Nuttall}\footnote{currently reachable on Instagram at
  \url{https://www.instagram.com/luke_is_frog/}} for providing a drawing of the
Netfarm mascot \textsl{Demeter}.

We would also like to thank \textsl{Robert Strandh} for providing advice on
optimising the Netfarm script machine and how Netfarm should interact with
client code, and \textsl{Gilbert Baumann} for also suggesting optimisations for
the script machine and several stylistic concerns, among other many other
insightful discussions.

\subsection{What to do next}

The boundaries that separate a Netfarm implementation and its host language are
not yet clear. Several questions remain, such as how much a Netfarm node must
know about the data it is verifying, and what it is expected to do for its
clients.

We forsee that a new distributed object system will have to be created, to
overcome flaws that were introduced to Netfarm through its age and attempts
to persue multiple goals. One such problem was that we chose the means of
interaction with the network to be sending and receiving data, which creates
an awkward split in mechanisms, between the creation of an object and objects
sending messages to each other.

As such, we would like to remind the reader that they should take care to
form their own opinions on how such a system should be structured from any
problems they have noticed, and that Netfarm is not the end-all of distributed
object systems. We would hope for something like Pierre-Joseph Proudhon's dream
in this quotation:

\begin{quotation}
  Je rêve une société où je serai guillotiné comme conservateur.
  
  (I dream of a world in which I would be executed as a reactionary.)
\end{quotation}

We hope the approach we have developed for Netfarm, of identifying and
constructing alternatives to many forms of centralisation, will be commonplace
and we will be the ones developing centralised systems.