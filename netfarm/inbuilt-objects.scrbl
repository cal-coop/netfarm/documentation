#lang scribble/base

@require["../spec-macros.scrbl"]
@title{Inbuilt objects}

Netfarm contains several @concept{inbuilt objects}, which are always
required to run a Netfarm system. These objects, for example, include schemas
for user objects, which are necessary for signature verification, and the
schema schema, which permits defining new schemas.

Inbuilt schemas do not have meaningful values for the @cl{documentation}
slot, because portable code could depend on the values otherwise, and
we would like to be able to modify the documentation if it is unclear
or incorrect.

@(define (inbuilt-object name schema . slots)
    (list (defthing name (list "instance of " schema) "Inbuilt object")
          (tabular #:sep (hspace 1)
                   #:column-properties '(right left)
                   slots)))
@(define (slot name value)
   (list (tt name) (tt value)))

@definitions{
@inbuilt-object[
 "inbuilt@schema" "inbuilt@schema"
 @slot["computed-slots"]{()}
 @slot["documentation"]{""}
 @slot["scripts"]{()}
 @slot["slots"]{(("computed-slots") ("documentation") ("scripts") ("slots"))}
]

Schemas are special objects, which describe the behaviour and storage
of their instances. Note that the schema @cl["inbuilt@schema"] describes
itself.
}

@definitions{
@inbuilt-object[
 "inbuilt@user" "inbuilt@schema"
 @slot["computed-slots"]{(("data"))}
 @slot["documentation"]{""}
 @slot["scripts" "ref:inbuilt@user-script"]
 @slot["slots"]{(("ecdh-key") ("sign-key"))}
]

Users are the primary form of authentication in Netfarm, as object
signatures are verified with the keys of the users attached with them.

A user object stores a public Ed52219 signing key (in @cl{sign-key}),
and a Curve25519 exchange key (in @cl{ecdh-key}).
}

@definitions{
@inbuilt-object[
 "inbuilt@script" "inbuilt@schema"
 @slot["computed-slots"]{()}
 @slot["documentation"]{""}
 @slot["scripts"]{""}
 @slot["slots"]{(("entry-points") ("methods") ("program") ("variables"))}
]

Scripts are "programs" which describe the behaviour of an object.
A script has entry points that describe internal procedures, methods
that describe external procedures, an octet vector encoding
instructions in the program, and a list of the initial values of its
global variables.
}

@definitions{
@inbuilt-object[
  "inbuilt@user-script" "inbuilt@script"
  @slot["entry-points"]{()}
  @slot["methods"]{(("add-datum" 2 0))}
  @slot["program"]{#(7 73 1 12 134 71 16 0 1 0 0 8 2 0 4 1 0 130 8)}
  @slot["variables"]{("data")}
]

This script can be assembled with

@lisp-code{
(define-script *user-add-datum-script* ("data")
  (:method "add-datum" 1)
  ;; Make the expected author list (just myself).
  self (list 1)
  ;; Get the sender's author list.
  sender object-authors
  ;; If they aren't equal, get out.
  equal (jump-cond 0 1 0 0) return
  ;; Otherwise, add their message as a computed value.
  (get-value 0) (get-env 1 0) add-computed-value return)
}
}

@definitions{
@inbuilt-object[
  "inbuilt@mirror-schema" "inbuilt@schema"
  @slot["computed-slots"]{()}
  @slot["documentation"]{""}
  @slot["scripts" "(ref:inbuilt@mirror-script)"]
  @slot["slots"]{()}
]

The schema of an object that can "reflect" messages, in a way that
makes the messages anonymous, by making the sender always the mirror
object.

For example, this object could be used in an authentication scheme where
one object has to challenge another to present a reference to
itself. Without a mirror, the other object could cheat by always
presenting the sender object; but with a mirror, it cannot do so.

This schema cannot be initialized by the user.
}

@definitions{
@inbuilt-object[
  "inbuilt@mirror" "inbuilt@mirror-schema"
]

The only instance of @cl["inbuilt@mirror-schema"].
}
