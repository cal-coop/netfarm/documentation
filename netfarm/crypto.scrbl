#lang scribble/base

@require["../spec-macros.scrbl"]
@require[scribble-math/dollar]

@title{Cryptographic operations}

@section{Hashing}

Netfarm uses cryptographic @concept{hashes} to ``name'' objects, so that they
can be referenced and retrieved by clients that do not already have them
stored. Hashes are also used in the process of signing objects.

An object is hashed by rendering it in the binary format, and then
hashing the resulting octet vector using the SHA-256 hash algorithm. Usually,
computed values are not included in the render, as computed values must not
affect object identity. Furthermore, when hashes are used in signing objects,
signatures are not included, as they would need to be already present in the
object to be signed, or break identity (by changing the resultant hash, or by
allowing signatures to be added or removed to an object while preserving hash
value).

@definitions{
@defun["hash-object"]{object @&key (emit-signatures @cl{t})}

Produce a binary hash. Note that inbuilt objects cannot be hashed, and thus
hashing them will signal an error. @cl{emit-signatures} will control if
signatures are included in the hash.

The most obvious inbuilt object that could not possibly be hashed is
@cl["inbuilt@schema"], which is its own schema. Of course, you could hash it by
referencing its inbuilt name and not its hash, but the renderer would have to
have a special case to use inbuilt objects' names instead of the result of
@cl{hash-object*}, which felt worse than disallowing hashing inbuilt objects
somehow.

@defun["hash-object*"]{object}

Produce a textual hash, equivalent to @cl{(bytes->base64 (hash-object object))}
for non-inbuilt objects, and in the form @cl["inbuilt@" @italic{name}] for
inbuilt objects.
}

@definitions{
@defmacro["with-hash-cache"]{(@&key (keep-old @cl{nil})) @&body body}

Cache hashes produced with both hashing functions in the body.

@definition-section["Arguments"]{
  If @cl{keep-old} is true (unevaluated), and there is already a hash cache
  present, the current hash cache is reused.
}

Hashing can result in accidentally quadratic runtime when traversing
graphs of objects and hashing them (such as saving a graph of
objects with the Netfarm client). @cl{with-hash-cache} will reduce
hashing operations that have already been done (usually by recursive
calls between hashing and the renderer) to hash-table lookups,
reducing that kind of pattern to roughly linear runtime.

@centered{
@image/width["5cm" #:scale 1.2]{images/quadratic-hashing.png}

Hashing a graph of objects naïvely requires @${O(n^2)} hashes.
}

Hashes used in signing are currently not cached (@todo{though they probably
  should be}.)

If objects that have been hashed are modified, then the results of
@cl{hash-object} and @cl{hash-object*} are undefined; but the dynamic extent of
@cl{with-hash-cache} should allow for fairly easy analysis of where hashes can
be cached.

Object hashes are stored in a @term{weak hash table}, so they will be removed
when the objects are garbage, and the garbage collector runs. The hash cache
is thread safe; one can use the next macros to transport caches between threads.
}

@definitions{
@defmacro["with-lexical-hash-cache"]{() @&body body}

Bind the hash caches in a way that allows for them to be closed over, and
restored using the local macro @cl{with-restored-lexical-hash-cache}.
This is intended to be used to share cached hashes between threads; which
takes some more deliberation to ensure correctness, but can be beneficial.

The Netfarm server uses a hash cache shared between all threads; and this is
safe as objects are never mutated inside the server.
}

@definitions{
@defmacro["with-restored-lexical-hash-cache"]{() @&body body}

Restore the hash caches from @cl{with-lexical-hash-cache}.
}

@section{Signing}

@definitions{
@defclass{keys}

A class containing keys used for cryptography.

@todo{Currently, we use the same class for private and public keys, and just
public keys. Should we split these cases up somehow?}
}

@definitions{
@defun{generate-keys}

Generate random keys.
}

@definitions{
@defreader["keys-exchange-private"]{keys}

@defreader["keys-exchange-public"]{keys}

@defreader["keys-signature-private"]{keys}

@defreader["keys-signature-public"]{keys}
}

@definitions{
@defun["object->keys"]{object}

Convert an instance of @cl{user} into a keys object. The keys object will not
have private keys.

@defun["keys->object"]{keys}

Convert a keys object into an instance of @cl{user}.
}

@definitions{
@defun["shared-key"]{ours theirs}

Generate a shared key for symmetric cryptography, using
@term{elliptic curve Diffie-Hellman}. @cl{ours} must have private keys, but
@cl{theirs} does not have to have private keys.
}

@definitions{
@defun["sign-object"]{object key}

Return a signature.

@defun["add-signature"]{object key user}

Add a signature to an object, authored by the provided user object, and
signed using the provided keys object.
}

@definitions{
@defun["verify-object-signatures"]{object}

@defun["verify-vague-object-signatures"]{object}

Verify all the signatures on an object or vague object, returning true if
all the signatures are valid, or false if any signatures are invalid.
}
