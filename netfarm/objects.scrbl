#lang scribble/base

@require["../spec-macros.scrbl"]

@title{Objects}

The Netfarm object system has objects, which are instances of schemas, which
are themselves objects.

This system maps well to the Common Lisp object system, and we can describe
schemas using classes that are instances of a special @term{metaclass}.

@definitions{
@defthing["netfarm-class" "" "Metaclass"]

The class of a Netfarm class.
}

@definitions{
@defprotoclass{object}

The class of a Netfarm object.

A Netfarm object also has a list of signatures, and a @concept{source} function,
that is called to retrieve referenced objects lazily.
}

@definitions{
@definitarg{:signatures}

@defaccessor["object-signatures"]{object}

The signatures of an object, stored as an association list of
user object-signature data pairs.
}

@definitions{
@definitarg{:source}

@defaccessor["object-source"]{object}

A function that takes an object name, and returns the object with that object
name. This function typically requests the object from a client.
}

@section{Slots}

Much like objects in the Common Lisp @term{meta-object protocol},
Netfarm objects are represented as vectors of slots; an object is
comprised of a vector of ``normal'' slots, and a vector of computed
slots.

Each Netfarm slot maps to a Common Lisp slot, but some Common Lisp slots
don't map to Netfarm slots.@margin-note{This may, for example, be useful to
  separate internal and external representations; it would be wise to
  represent a mapping of objects as a hash table in Lisp, but provide an
  association list for Netfarm to work with.}

To avoid some potential non-determinism in how slot lists of schemas
expressed with Common Lisp classes are ordered when read by Netfarm,
the slots of classes defined in Common Lisp are sorted by @cl{string<}
on their names in slot vectors.

@definitions{
@defclass["netfarm-slot" "standard-direct-slot-definition"]

The class of @term{direct slot definitions} for Netfarm slots.

@defclass["netfarm-effective-slot" "standard-effective-slot-definition"]

The class of @term{effective slot definitions} for Netfarm slots.
}

@definitions{
@definitarg{:netfarm-name}

@defreader["slot-netfarm-name"]{slot-definition}

The name of a slot, a string or @cl{nil} if the slot should not be present
in the Netfarm object. This defaults to the downcased name of the
@cl{slot-definition-name} of the slot.

Multiple slots of a class cannot have the same name.
}

@definitions{
@defreader["netfarm-slot-position"]{effective-slot-definition}
}

@definitions{
@definitarg{:renderer}

@defreader["slot-renderer"]{slot}

@definitarg{:parser}

@defreader["slot-parser"]{slot}

The names of functions which are used to render and parse slot values.
The parser is called when calling @cl{apply-class}, and is used to
parse a Netfarm representation into the preferred representation of
your program. The renderer is called when calling @cl{render-object}
or @cl{binary-render-object}, and is used to render your representation
as a Netfarm representation.

For all values @cl{v}, it is expected that
@cl{(netfarm-equal (funcall (slot-renderer slot) (funcall (slot-parser slot) v)) v)};
the renderer and parser should round trip. If this is not the case,
weird things can happen when processing objects, and you should
instead use @cl{:netfarm-name} to create duplicate slots with your
preferred representation, preserving the Netfarm representation in the
original slot.
}

@definitions{
@defgeneric["fix-up-copied-object"]{object}

Fix up the slots of an object that has been loaded (called from
@cl{apply-class}). This should be used like @cl{initialize-instance};
you should add @cl{:after} methods to it, and should probably be used
to reproduce your preferred representation slots from the Netfarm
representation slots.
}

@section{Scripts}

@definitions{

@defprotoclass{script}

@definitarg{:scripts}

@defreader["netfarm-class-scripts"]{class}

The scripts of a class.
}

@section{Inheritance}

Slots are inherited as usual for Common Lisp.

The scripts of a class are the union of the direct scripts and the scripts
of the superclasses, with duplicates removed, preserving the first position
a script is in.
