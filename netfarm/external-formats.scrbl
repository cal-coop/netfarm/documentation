#lang scribble/base

@require["../spec-macros.scrbl"]
@title{External formats}

Objects are usually @concept{serialized} in order to transmit them to another
client that does not share memory with the origin of the object, and that
client must then @concept{deserialize} the serialized form to produce a usable
object.

In Netfarm, serialization is done by converting an object to a
@concept{vague object}, which has as much context of the object
removed as possible, which can then be serialized into a character or
octet vector. These vectors can then be deserialized by deserializing
the vector into a vague object, and then re-introducing the context
that the origin of the object supplied.

Objects and slot values are rendered and parsed using different functions;
all the object functions will render the contents of an object, whereas the
value functions will render a reference to the object. 
The octet-vector representation is also used for generating and verifying
@term{signatures}.

@section{Vague objects}

@definitions{
@defprotoclass{vague-object}

The class of a @term{vague object}.
}

@definitions{
@defreader["vague-object-values"]{vague-object}

@defreader["vague-object-computed-values"]{vague-object}

The storage vectors for object values and computed values, respectively.
Storage vectors are always of the type @cl{simple-vector}.
}

@definitions{
@defreader["vague-object-metadata"]{vague-object}

A hash table mapping metadata names to metadata values. Currently, only
the @cl{schema} metadata is used, and really we should get rid of metadata.

The "metadata" storage is an artifact of when Netfarm was going to maintain
arbitrary hash tables of values, and not a class-based object system. It
probably can be removed.

@defreader["vague-object-schema-name"]{vague-object}

The schema name of the vague object. If no schema name was provided, this
will signal an error.
}

@definitions{
@defun["apply-class"]{vague-object class}

Recontextualize a @term{vague object} by ``applying'' a class to it, producing
an object.
}

@section{Character format}

@todo{We would have a BNF diagram, like in Binary format, but the character format
      is pretty terrible. Maybe we should just remove the character format?}

@definitions{
@defun["render"]{value @&optional stream}
 
Render a value. If a stream is given, then write the value to that stream, else
return the rendered value as a string.
}

@definitions{
@defun["render-object"]{object @&optional stream}

Render an object. If a stream is given, then write the object to that stream, else
return the rendered object as a string.
}

@definitions{
@defun["parse"]{stream-or-string}

Parse a value from a stream or a string.
}

@definitions{
@defun["parse-block"]{string @&key source name}

Parse an object from a string. The returned vague object has the supplied
@term{source} and name.
}

@section{Binary format}

@bnf[
  @rule["byte"       @list[@tt{00} " ... " @tt{FF}]]
  @rule["byte-count" "byte"]

  @rule["integer" "byte-count byte*"]
  @rule["length"  "integer"]

  @tag-rule["string"      "01" "length byte*"]
  @tag-rule["byte-vector" "02" "length byte*"]
  @tag-rule["positive-integer" "03" "integer"]
  @tag-rule["negative-integer" "04" "integer"]
  @tag-rule["list"      "05" "length value*"]
  @tag-rule["reference" "06" "length byte*"]
  @tag-rule["true"      "07" ""]
  @tag-rule["false"     "08" ""]
  @tag-rule["slot-unbound" "09" ""] 

  @rule["value" "string" "byte-vector"
                "positive-integer" "negative-integer"
                "list" "reference"
                "true" "false"]
  @rule["slot"  "value" "slot-unbound"]
  @rule["slot-vector" "length slot*"]

  @rule["signature-data" "byte × 64"]
  @rule["user"           "byte × 64"]
  @rule["signature"      "user signature-data"]
  @rule["signatures"     "length signature*"]

  @rule["metadata-element" "string value"]
  @rule["metadata"         "length metadata-element*"]

  @rule["slots" "slot-vector"]
  @rule["computed-slots" "slot-vector"]
  @rule["object" "signatures metadata slots computed-slots"]
]

Strings, byte vectors, slot vectors, and references have as many bytes as the
length represents, lists have as many values as the length represents, and
metadata and signatures have as many elements as the length represents.

The integer 0 must be represented as a positive-integer, and integers must
be represented with as few bytes as possible; for example, 12,345 must be
represented as @tt{03 02 30 39}, and not @tt{03 03 00 30 39} or with
more zero bytes.

@definitions{
@defun["binary-render"]{value @&optional function}

Render a value. If a function is given, then write the value by calling it with
octet vectors that it should write, else return the rendered value as an octet
vector.
}

@definitions{
@defun["binary-render-object"]{object @&key function
  (emit-computed-values @cl{t}) (emit-signatures @cl{t})}

Render the contents of an object.

@definition-section["Arguments"]{
  If @cl{emit-computed-values} is false, computed values will not be included
  in the rendered object. This corresponds to removing computed-slots from the
  rule for object syntax.
  
  If @cl{emit-signatures} is false, signatures will not be included in the
  rendered object. This corresponds to removing signatures from the
  rule for object syntax.
  
  If either argument is not true, then @cl{binary-parse-block} will not be able
  to parse the object. Usually, you do not want to change either of these
  arguments, unless you are implementing hashing, in which case they should both
  be false.

  If a @cl{function} is provided, it will be called repeatedly with byte vectors
  to write. Otherwise, @cl{binary-render-object} will render the object into a
  byte vector, and return that.
}
}

@definitions{
@defun["binary-parse"]{function-or-vector}

Parse a value from a function (called with no arguments, which returns an octet
each call) or a byte vector.
}

@definitions{
@defun["binary-parse-block"]{function-or-vector @&key source name}

Parse an object from a function (as previously described for @cl{binary-parse})
or a byte vector.
}
