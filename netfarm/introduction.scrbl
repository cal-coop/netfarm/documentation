#lang scribble/base

@require["../spec-macros.scrbl"]
@title{Netfarm}

The second component that was developed was the @concept{Netfarm object system}.
The Netfarm object system introduces a simple object model, as well as techniques
for serializing and deserialising objects.

The part of the Netfarm system that implements an object system is also named
Netfarm. It may be helpful to describe this component as the ``Netfarm object
system'', and the entire system as the ``Netfarm suite,'' or words to those
meanings.

As an example of how Netfarm enables its users to exchange objects with little
friction, we will imagine that there are two car enthusiasts, Adrian and Bob,
who want to discuss their cars. Here is a class definition for a car that they
could have used in a client program:

@lisp-code{
(defclass car ()
  ((colour :initarg :colour :reader car-colour)
   (year   :initarg :year   :reader car-year)
   (make   :initarg :make   :reader car-make)
   (model  :initarg :model  :reader car-model))
  (:metaclass netfarm:netfarm-class))
}

This class functions identically to any other class defined using the
@term{Common Lisp Object System}:

@lisp-code{
Adrian> (make-instance 'car :year 1952 :make "Studebaker"
                            :model "Starlight" :colour "blue")
#<CAR>
Adrian> (defvar *my-car* *)
*MY-CAR*
Adrian> (car-model *my-car*)
"Starlight"
}

We will discuss how Adrian can send that description of a car to Bob later.

@include-section["objects.scrbl"]
@include-section["inbuilt-objects.scrbl"]
@include-section["external-formats.scrbl"]
@include-section["crypto.scrbl"]
