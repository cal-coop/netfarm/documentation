#lang scribble/base

@require["../spec-macros.scrbl"]
@title{Netfarm server}

The @concept{Netfarm server} is a decentralise2 system, which retrieves a
partition of all the objects available to it, runs scripts for them, and makes
the objects and computed values available to its connections.

In the introduction to decentralise2, we argued that the terms ``client''
and ``server'' do not have enough meaning to be useful when discussing Netfarm
software; but we ended up calling this module a ``server''. 

@definitions{
@defclass["netfarm-system"]{standard-system}

The class of a Netfarm server.
}

@include-section["database.scrbl"]
