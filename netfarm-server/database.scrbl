#lang scribble/base

@require["../spec-macros.scrbl" "../bibliography.rkt"]
@require[scribble-math/dollar]
@title{Database protocol}

@section{Implementations of the database protocol}

@centered{
  @image/width["10cm" #:scale 0.25]{images/netfarm-server-classes.png}
  
  A diagram of Netfarm server classes, including system and
  database mixin classes. Shaded classes are instantiable, and not
  shaded classes are not instantiable. Dashed classes are decentralise2
  classes included for completion.
}

@definitions{
@defclass["memory-system"]{netfarm-system}

A system that stores everything in memory. Compare this to decentralise2's
@cl{memory-database-mixin}.

}

@definitions{
@defclass["simple-memory-system"]{netfarm-system}

A system that stores everything in memory, using the simplest feasible
implementations of the database protocol.
}

@subsection{Filesystem database}

@definitions{
@defclass{filesystem-mixin}

A mixin that stores objects in a filesystem.

Objects are stored in a ``trie'' constructed of directories, to avoid slowdowns
that occur on some file systems, should one directory contain many files.
For each object, three files are stored:
@itemlist[
@item{A @tt{.object} file that contains the object, and computed values
  appended to the end of the object,}
@item{A @tt{.affected-by} file that contains a list of object names that
  this object has been affected by, and}
@item{A @tt{.affects} file that contains a list of object names that this
  object affects.}
]

Some files are also stored in the storage directory:
@itemlist[
@item{A @tt{dependency-graph} file that stores the @term{dependency graph} as an
  association list,}
@item{A @tt{interesting-set} file that stores the @term{other interesting set},}
@item{A @tt{unpresentable-set} file that stores the inverse of the
  @term{presentable set} (as it would be smaller than the presentable set),}
@item{@todo{A @tt{fulfilled-dependencies} file that stores object names
    that have had all dependencies fulfilled, but have not been further
    processed}, and}
@item{@todo{A @tt{script-machines} file that stores information about the
    script machines that need to be run.}}
]

@definitarg{:directory}

The directory that objects should be stored in.

}

@subsection{Caching}

@definitions{
@defclass{caching-mixin}

A mixin that caches some objects in memory, but defers to another database
implementation when it does not have an object stored.

@definitarg{:cache-size}

The maximum rendered size of objects that can be cached at once, in bytes.
This defaults to 100 megabytes (@${10^8} bytes).

@definitarg{:fallback-vague-object-size}

The size that vague objects should be assumed to take when rendered, should
the real database not provide a size.

}

@section{Vague object storage}

@definitions{
@defgeneric["put-vague-object"]{system vague-object}

@defgeneric["get-vague-object"]{system name}

}

@definitions{
@defgeneric["map-vague-objects"]{function system}

}

@definitions{
@defgeneric["vague-object-stored-p"]{system name}

}

@definitions{
@defgeneric["count-vague-objects"]{system}

}

@subsection{Presentable set storage}

@definitions{
@defaccessor["presentable-name-p"]{system name}

}

@definitions{
@defgeneric["count-presentable-objects"]{system}

}

@subsection{Side effect storage}

@definitions{
@defgeneric["apply-side-effect"]{system name cause side-effect-type @&key}

}

@definitions{
@defgeneric["map-computed-values-caused-by"]{function cause-name system}

}

@definitions{
@defgeneric["objects-affected-by"]{system name}
Return a list of the object hashes affected by a given object.

@defgeneric["objects-affecting"]{system name}
Return a list of the object hashes which affect a given object.
}

@definitions{
@defgeneric["objects-affecting-hash"]{system name @&key test}
Compute a hash value for the objects-affecting list, by
@${ \prod_{a \in \text{affecting}} a \pmod{2^{256}} }

If a @cl{test} function is provided, only object names which satisfy the
function will be hashed.

When a test is not provided, hashing can be implemented incrementally,
but the default method computes it from the entire objects-affecting
list. (Recall multiplication and modular multiplication are
commutative - a database could store the last hash, and then compute
@${h := h \times a \pmod{2^{256}}} when adding @${a}.)

(We were initially going to reduce @cl{logxor} over the set, but
bitwise exclusive-or, among all additive groups, can be attacked by
@~cite[birthday-problem]. It is unclear what such an attack could achieve,
but we are going to avoid it by using a multiplicative group instead.)
}

@section{Other interesting set}

@definitions{
@defgeneric["add-other-interesting-block"]{system name}

@defgeneric["remove-other-interesting-block"]{system name}

@defgeneric["other-interesting-block-p"]{system name}

}

@section{Dependency graph}

A Netfarm server maintains a @concept{dependency graph}, consisting of a set of
@concept{dependency edges}, which each store the name of a dependent (a string),
the name of a dependency (another string), and a type (a keyword).

For the avoidance of doubt (because we frequently got confused by the names):
further processing of a @concept{dependent} object is not possible until a
@concept{dependency} object is retrieved.

All functions that take edge types except for @cl{add-dependency-edge} also
accept the type @cl{:all} to retrieve all edges, regardless of type.

@definitions{
@defgeneric["add-dependency-edge"]{system type dependent dependency}

}

@definitions{
@defgeneric["remove-dependents"]{system dependency}

}

@definitions{
@defgeneric["map-dependents"]{function system type dependency}

@defgeneric["map-dependencies"]{function system type dependencies}

@defgeneric["map-dependency-edges"]{function system}
}

@definitions{
@defgeneric["no-dependencies-p"]{system type name}

}

@definitions{
@defclass{dependency-list-mixin}

A mixin that implements a dependency graph by storing a list of its edges.
This mixin may be slower than @cl{dependency-table-mixin}, but it is
easier to reason about, as it does not do any tricky indexing.
}

@definitions{
@defclass{dependency-table-mixin}

A mixin that implements a dependency graph by storing the edges in hash
tables. This may look up dependencies and dependents more efficiently than
@cl{dependency-table-mixin}, especially with many dependencies to manage.
}
