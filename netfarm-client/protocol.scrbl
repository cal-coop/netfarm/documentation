#lang scribble/base

@require["../spec-macros.scrbl"]

@title{Protocol}

@definitions{
@defclass{client}

@definitarg{:bootstrap-uris}

A list of URIs to connect to in order to find objects or other nodes,
"bootstrapping" object location.
}

@definitions{
@defun["save-object"]{client object}

@defun["save-single-object*"]{client object}

@defun["save-single-object"]{client object}
}

@definitions{
@defun["find-object"]{client hash @&key timeout follow-updates?}

@defun["find-object*"]{client hash @&key follow-updates?}
}

@definitions{
@defgeneric["object-requires-real-counters-p"]
}
