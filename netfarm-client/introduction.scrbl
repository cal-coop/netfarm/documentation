#lang scribble/base

@require["../spec-macros.scrbl"]
@title{Netfarm client}

Netfarm uses a small network protocol to exchange objects and updates
to objects. This protocol is implemented on decentralise2, but it is
not necessary at all for a Netfarm implementation to use any given
networking protocol. The basic requirements of the network protocol
are:

@itemlist[
@item{Retrieving objects by hash, and storing objects.}
@item{Retrieving other values with special names (i.e. special blocks
in decentralise2).}
@item{Retrieving a listing of all the hashes of the objects a node has
stored.}
@item{A publish-subcribe mechanism, ideally using named channels which
can be subscribed to, and which broadcasts some object metadata that
belongs to that channel.}
@item{When a node holds only part of the state of the network, such as
in a @term{distributed hash table}, a mechanism should be provided to
allow retrieving @term{other interesting objects}, which would not
otherwise be part of that partition.}
]

Any other extensions or mechanisms are not necessary to communicate
Netfarm objects, but some may be very useful. One "glaring" omission
in decentralise2 is chunking; a user may wish to begin using partially
transmitted information before it has all been rendered and received.

The @cl{netfarm-client} system provides functions to read and write
Netfarm objects from a decentralise client, as well as a client class
which can search through a network for an object.

Continuing our car example, Adrian can create a client and use it to
save his car:

@lisp-code{
Adrian> (defvar *client* 
                (make-instance 'netfarm-client:client 
                               :bootstrap-uris '("netfarm:a-server"))) 
*CLIENT* 
Adrian> (netfarm-client:save-object *client* *my-car*) 
"Lb6TI6j/GdkLsyCqt20xZhz7PohttpXejZTCKQEjT58="
}

@cl{save-object} returns the hash of the object, which can be used by
Bob to retrieve the object. Having to pass around a hash is not very
convienent, and we can attach our car to some other object Bob may
already know about, but we will begin by using this hash to retrieve
the car.

@lisp-code{
Bob> (netfarm-client:find-object *client* 
      "Lb6TI6j/GdkLsyCqt20xZhz7PohttpXejZTCKQEjT58=") 
#<CAR> 
Bob> (describe *) 
#<CAR> 
  [standard-object] 
 
Slots with :INSTANCE allocation: 
  ... 
  COLOUR                         = "blue" 
  YEAR                           = 1952 
  MAKE                           = "Studebaker" 
  MODEL                          = "Starlight" 
}

@include-section["protocol.scrbl"]
